// auto-executing function for encapsulation
(function () {

    /**
     * Player class for paddle, score, and wins
     */
    class Player {
        /**
         * Creates a new Player object.
         * @param paddle A Paddle object for the player.
         */
        constructor(paddle) {
            this.paddle = paddle;
            this.score = 0;
            this.wins = 0;
        }

        /**
         * Updates the paddle's current state.
         */
        update() {
            this.paddle.update();
        }

        /**
         * Increment score by one.
         */
        scoreUp() {
            this.score++;
        }

        /**
         * Decrement score by one.
         */
        scoreDown() {
            this.score--;
        }
    }

    /**
     * Ball class
     */
    class Ball extends TrailedCircle {
        /**
         * Creates a new Ball object.
         * @param context A 2D canvas context
         * @param x X value
         * @param y Y value
         * @param radius Radius in pixels
         * @param direction Movement angle in radians
         * @param speed Movement speed in pixels
         * @param angle Facing angle
         * @param rotation Rotation per update in radians
         * @param color Color value (string or RgbaColor Object)
         * @param trailColor Color value (string or RgbaColor Object)
         * @param trailMultiplier Multiplier for trail length
         * @param shadow Shadow object
         */
        constructor(context, x, y, radius, direction, speed, angle, rotation, color, trailColor, trailMultiplier = 5,
                    shadow = null) {

            super(context, x, y, radius, direction, speed, angle, rotation, color, trailColor, trailMultiplier, shadow);
            this.baseSpeed = speed;
            this.speedMultiplier = Ball.speedMultiplier.normal;
        }

        /**
         * Determines if the ball is currently colliding with top or bottom walls.
         * @returns {boolean} Returns true if such a collision is occurring, else false.
         */
        isWallCollision() {
            let destinationY = this.y + this.dy;
            return destinationY <= 0 || destinationY > canvas.height;
        }

        /**
         * Updates the ball's position, handles collision checks, and draws the ball.
         */
        update() {
            this.updateAngle();

            // if top or bottom wall collision, bounce ball
            if (this.isWallCollision()) {
                this.setDirection(this.direction * -1)
            }

            let paddle = nearestPaddle(new Point(this.x, this.y));

            // if axis-aligned bounding box collision between ball and paddle occurs, handle collision
            if (isAABBCollision(this.x + this.dx, this.y + this.dy, this.radius, this.radius, paddle.x,
                paddle.y, paddle.width, paddle.height)) {

                // use an adjusted center y-value for the paddle. this reduces the severity of direction change when
                // the ball bounces
                let adjustedPaddleY = paddle.getMiddleY() + (this.y - paddle.getMiddleY()) * .75;

                // set speedMultiplier to fast if collision is on paddle edge, else set to normal
                if (this.y < paddle.y + paddle.height * paddle.edgeProportion
                    || this.y >= paddle.y + paddle.height * (1 - paddle.edgeProportion)) {

                    this.speedMultiplier = Ball.speedMultiplier.fast;
                } else {
                    this.speedMultiplier = Ball.speedMultiplier.normal;
                }

                this.setSpeed(this.baseSpeed * this.speedMultiplier);

                // set bounce angle
                this.setDirection(angleRadians(paddle.getMiddleX(), adjustedPaddleY, this.x, this.y));
                paddle.randomizeOffset();
            } else {
                this.setSpeed(this.baseSpeed * this.speedMultiplier);
            }

            this.move();
            this.draw();
        }

        /**
         * Moves the ball back to the middle of the playing area and chooses a random vertical placement within
         * constraints. Using chosen serve direction, the ball is served using a random angle within constraints.
         * @param side Left or right side.
         */
        reset(side = sides.randomSide()) {
            let maxDirectionAngle = toRadians(20);
            let direction = randomFloatInRange(-maxDirectionAngle, maxDirectionAngle);

            if (side === sides.left) {
                direction += Math.PI;
            }

            this.setDirection(direction);
            let verticalPlacementRatio = .3;
            this.speedMultiplier = Ball.speedMultiplier.slow;

            this.x = Math.round(canvas.width / 2);
            this.y = Math.round(canvas.height / 2 + canvas.height / 2 * randomFloatInRange(-verticalPlacementRatio, verticalPlacementRatio));

            this.trailColor.randomize();
        }
    }

    Ball.speedMultiplier = {
        slow: .9,
        normal: 1,
        fast: 1.1,
    };

    /**
     * Paddle class
     */
    class Paddle extends Rectangle {
        constructor(context, x, y, width, height, direction, speed, color, side, isComputerControlled, shadow = null) {
            super(context, x, y, width, height, 0, speed, color, shadow);
            this.side = side;
            this.isComputerControlled = isComputerControlled;
            this.targetPoint = null;
            this.edgeProportion = .18;
            this.speed = speed;
            this.targetOffset = 0;
            this.randomizeOffset();
        }

        /**
         * Updates the paddle's current state. Moves the paddle if computer controlled.
         */
        update() {
            if (this.isComputerControlled) {
                let ball = nearestBall(new Point(this.getMiddleX(), this.getMiddleY()), true);
                let targetDifference = null;

                // continue if a ball is found
                if (ball) {
                    // ball and paddle distance calculation
                    let xDistance = Math.abs(this.getMiddleX() - ball.x);
                    let middleDistance = Math.abs(this.getMiddleX() - canvas.width / 2);

                    // if ball is on the paddle's side of the playing area
                    if (xDistance < middleDistance) {
                        // offset used to slightly randomize target point, avoiding straight hits occurring every time.
                        this.targetPoint = new Point(this.x, ball.y + this.targetOffset);
                    }
                }

                if (this.targetPoint) {
                    // canvas edge clipping check
                    if (this.targetPoint.y < this.height / 2) {
                        this.targetPoint.y = this.height / 2;
                    } else if (this.targetPoint.y > canvas.height - this.height / 2) {
                        this.targetPoint.y = canvas.height - this.height / 2;
                    }

                    targetDifference = this.targetPoint.y - this.getMiddleY();
                }

                // move paddle toward target
                if ((targetDifference || targetDifference === 0) && Math.abs(targetDifference) >= this.speed) {
                    this.y += this.speed * Math.sign(targetDifference);
                } else if ((targetDifference || targetDifference === 0)) {
                    this.y += targetDifference;
                }
            }
            this.draw();
        }

        /**
         * Randomizes the paddle's target point.
         */
        randomizeOffset() {
            let maxOffset = this.height * .45;
            this.targetOffset = randomFloatInRange(-maxOffset, maxOffset);
        }
    }

    // canvas variables
    const canvas = document.querySelector('#pongCanvas');
    const context = canvas.getContext('2d');

    // menu variables (HTML elements)
    const gameMenuElement = document.querySelector('#gameMenu');
    const gameMenuOffsetElement = document.querySelector('#gameMenuOffset');
    const gameMenuCollapseButton = document.querySelector('#gameMenuCollapse');
    const winScoreInput = document.querySelector('#winScore');
    const minBallsInput = document.querySelector('#minBalls');
    const maxBallsInput = document.querySelector('#maxBalls');

    // side selector elements
    const sideLeftRadio = document.querySelector('#sideLeft');
    const sideRightRadio = document.querySelector('#sideRight');

    // ball setting inputs
    const speedMultiplierInput = document.querySelector('#speedMultiplierInput');
    const speedMultiplierMinDisplay = document.querySelector('#speedMultiplierMin');
    const speedMultiplierSlider = document.querySelector('#speedMultiplierSlider');
    const speedMultiplierMaxDisplay = document.querySelector('#speedMultiplierMax');
    const startMaxBallsInput = document.querySelector('#startMaxBalls');
    const persistentBallsInput = document.querySelector('#persistentBalls');

    // button elements
    const resetDefaultButton = document.querySelector('#resetDefaultButton');
    const demoButton = document.querySelector('#demoButton');
    const playButton = document.querySelector('#playButton');

    // pause menu elements
    const pauseMenuElement = document.querySelector('#pauseMenu');
    const resumeButton = document.querySelector('#resumeButton');
    const quitButton = document.querySelector('#quitButton');

    let requestAnimationFrameId = 0;

    /**
     * Mouse position
     * @type {{x: null, y: null}} Mouse x and y coordinates
     */
    const mouse = {
        x: null,
        y: null
    };

    /**
     * Side data for playing area
     * @type {{isRight(*=): *, isLeft(*): *, left: string, opposite(*): *, right: string, randomSide(): *}}
     */
    const sides = {
        left: 0,
        right: 1,

        /**
         * Get opposite side value
         * @param side Side value
         * @returns {number} Returns opposite side. Example, if value for left side entered, value for right side
         *     returned
         */
        opposite(side) {
            if (side === this.left) {
                return this.right;
            }
            return this.left;
        },
        /**
         * Get random side value
         * @returns {number} Returns left or right side value randomly.
         */
        randomSide() {
            let choice = randomInt(2);
            if (choice === 0) {
                return this.left;
            }
            return this.right;
        },
        /**
         * Determines if passed in coordinate point is on the left side of the playing area
         * @param point A Point object
         * @returns {boolean} Returns true if the point is on the left side, else false
         */
        isLeft(point) {
            return point.x < canvas.width / 2;
        },
        /**
         * Determines if passed in coordinate point is on the right side of the playing area
         * @param point A Point object
         * @returns {boolean} Returns true if the point is on the right side, else false
         */
        isRight(point) {
            return !this.isLeft(point);
        }
    };

    /**
     * UI-related variables and functions
     */
    const ui = {
        fontSize: 0,
        fontXModifier: .06,
        fontYModifier: .09,
        textColor: new RgbaColor(255, 255, 255),
        /**
         * Sets and applies default font size
         */
        init() {
            this.fontSize = Math.round(canvas.height * .062);
            this.applyFontSize();
        },
        /**
         * Applies the specified font size to the canvas context. If no font size specified, default font size is used
         * @param pixels Font size in pixels.
         */
        applyFontSize(pixels = this.fontSize) {
            context.font = pixels + 'px Arcade_I';
        },
        /**
         * Draws the UI
         */
        draw() {
            context.fillStyle = this.textColor.toString();
            this.displayScoreText();

            for (let i = 0; i < game.players.length; i++) {
                let player = game.players[i];
                if (player.score >= game.winScore) {
                    this.displayWinnerText(player.paddle.side);
                }
            }

            if (game.isPaused) {
                this.displayPausedText();
                pauseMenuElement.classList.remove('d-none');
            } else {
                pauseMenuElement.classList.add('d-none');
            }
        },
        /**
         * Displays the player score text
         */
        displayScoreText() {
            this.applyFontSize();
            for (let i = 0; i < game.players.length; i++) {
                let player = game.players[i];
                let x;
                let y = Math.round(canvas.height * ui.fontYModifier);

                if (player.paddle.side === sides.left) {
                    x = (canvas.width / 2 - canvas.width * ui.fontXModifier) - context.measureText(player.score).width;
                } else {
                    x = Math.round(canvas.width / 2 + canvas.width * ui.fontXModifier);
                }
                context.fillText(player.score, x, y);
            }
        },
        /**
         * Displays a 'paused' message
         */
        displayPausedText() {
            this.applyFontSize(this.fontSize * 1.25);
            let text = 'PAUSED';
            let x = Math.round(canvas.width / 2 - context.measureText(text).width / 2);
            let y = canvas.height * .3;
            context.fillText(text, x, y);
        },
        /**
         * Displays a 'winner' message
         * @param side Side value for which side to display the text
         */
        displayWinnerText(side) {
            this.applyFontSize(this.fontSize * .6);
            let text = 'WINNER!';
            let offset = canvas.width * .1;
            let x = offset;
            let y = canvas.height * .06;
            if (side === sides.right) {
                x = canvas.width - offset - context.measureText(text).width;
            }
            context.fillText(text, x, y);
        },
        /**
         * Draws the UI
         */
        update() {
            this.draw();
        },
        /**
         * Sets menu visibility
         * @param isVisible A boolean value for menu visibility
         */
        setMenuVisible(isVisible) {
            let className = 'd-none';
            if (isVisible) {
                gameMenuElement.classList.remove(className);
            } else {
                gameMenuElement.classList.add(className);
            }
        }
    };

    /**
     * Core game functionality
     */
    const game = {
        isPaused: false,
        speedMultiplierMin: .8,
        speedMultiplierMax: 1.2,
        defaultSpeedMultiplier: 1,
        speedMultiplier: 0,
        defaultMinBalls: 1,
        minBalls: 0,
        defaultMaxBalls: 10,
        maxBalls: 0,
        defaultIsStartMaxBalls: false,
        isStartMaxBalls: false,
        defaultIsPersistentBalls: false,
        isPersistentBalls: false,
        defaultWinScore: 10,
        winScore: 0,
        players: [],
        balls: [],
        paddles: [],
        dividerProportion: .002,
        isPlay: false,
        isDemo: true,
        initialServeDelayMs: 500,
        endTimeDelayMs: 1000,
        colors: {
            ball: new RgbaColor(255, 255, 255),
            paddle: new RgbaColor(255, 255, 255),
            divider: new RgbaColor(248, 248, 255),
        },
        /**
         * Reads and applies values from game options menu. Used before a game starts.
         */
        applyMenuValues() {
            this.minBalls = minBallsInput.value;
            this.maxBalls = maxBallsInput.value;
            this.isStartMaxBalls = startMaxBallsInput.checked;
            this.isPersistentBalls = persistentBallsInput.checked;
            this.winScore = winScoreInput.value;

            this.speedMultiplier = Math.min(Math.max(this.speedMultiplierMin, speedMultiplierInput.value), this.speedMultiplierMax);
        },
        /**
         * Sets game to default state.
         */
        init() {
            this.applyMenuValues();
            this.balls = [];
            this.paddles = [];
            this.players = [];
        },
        /**
         * Starts a new game.
         * @param isDemo If true, a computer-vs-computer demo plays instead of normal gameplay.
         */
        play(isDemo = false) {
            cancelAnimationFrame(requestAnimationFrameId);
            resize();

            game.isPlay = true;
            game.isDemo = isDemo;
            game.isPaused = false;
            game.init();
            game.createGameContent();

            animate();
            setTimeout(game.addBalls, game.initialServeDelayMs);
        },
        /**
         * Sets the game to a paused state. The animate() function reads this and reacts accordingly.
         */
        pause() {
            game.isPaused = true;
        },
        /**
         * Resumes from a paused state. The animate() function reads this and reacts accordingly.
         */
        unpause() {
            game.isPaused = false;
            animate();
        },
        /**
         * Starts a new computer-vs-computer demo.
         */
        startDemo() {
            game.play(true);
        },
        /**
         * Draws the divider down the middle of the playing area.
         */
        draw() {
            drawRect(context, (canvas.width * (1 - this.dividerProportion)) / 2, 0,
                canvas.width * this.dividerProportion, canvas.height, this.colors.divider.toString(), true);
        },
        /**
         * Updates gameplay components. Components call their draw methods accordingly.
         */
        update() {
            this.draw();

            // update balls
            for (let i = 0; i < this.balls.length; i++) {
                let ball = this.balls[i];
                ball.update();

                if (ball.x + ball.radius < 0) {
                    this.handleScore(sides.right, ball);
                } else if (ball.x - ball.radius > canvas.width) {
                    this.handleScore(sides.left, ball);
                }
            }

            // update players
            for (let i = 0; i < this.players.length; i++) {
                this.players[i].update();
            }
        },
        /**
         * Adds balls to the playing area. If ball count would exceed max allowable balls, balls are added up to the
         * max value.
         * @param balls Number of balls. If no value given, value determined by the 'isStartMaxBalls' boolean.
         * @param side Side value for serve direction.
         */
        addBalls(balls = game.isStartMaxBalls ? game.maxBalls - game.balls.length : game.minBalls - game.balls.length, side) {

            // set balls to max allowable balls if count exceeds max allowable
            balls = Math.min(balls, game.maxBalls - game.balls.length);

            let ballSpeed = Math.round(canvas.width * 0.013 * game.speedMultiplier);
            let ballRadius = Math.round(Math.min(canvas.width, canvas.height) * .009);

            // add balls up to limit
            for (let i = 0; i < balls; i++) {
                let trailColor = new RgbaColor(255, 255, 255, .9);
                let ball = new Ball(context, 0, 0, ballRadius, 0, ballSpeed, 0, 0,
                    game.colors.ball, trailColor);

                if (side) {
                    ball.reset(side);
                } else {
                    ball.reset();
                }
                game.balls.push(ball);
            }
        },
        /**
         * Creates players and their paddles.
         */
        createGameContent() {
            let height = Math.round(canvas.height * .12);
            let width = Math.round(canvas.width * .009);
            let paddleSpeed = Math.round(Math.min(canvas.width, canvas.height) * .015);
            let color = this.colors.paddle;

            // create paddles and players
            for (let i = 0; i < 2; i++) {
                let x = Math.round(canvas.width * .065);
                let y = Math.round(canvas.height / 2 - height / 2);
                let isComputerControlled = game.isDemo || i === 0 && sideRightRadio.checked || i === 1 && sideLeftRadio.checked;
                let side = null;

                // set side to left or right
                if (i === 0) {
                    side = sides.left;
                } else {
                    side = sides.right;
                }

                // set y to current mouse y if paddle is player-controlled
                if (!isComputerControlled) {
                    y = mouse.y;
                }

                // if right side, set x to correct right side value
                if (side === sides.right) {
                    x = canvas.width - x - width;
                }

                let paddle = new Paddle(context, x, y, width, height, 0, paddleSpeed, color,
                    side, isComputerControlled);

                this.paddles.push(paddle);
                this.players.push(new Player(paddle));
            }
        },
        /**
         * Determines if passed in score value is a winning score (>= win value).
         * @param score A score value.
         * @returns {boolean} Returns true if the score values is >= win value.
         */
        isWin(score) {
            return score >= game.winScore;
        },
        /**
         * Handles operations after a game has finished. Starts a new background demo, makes the menu visible, and sets
         * the menu offset to hidden.
         */
        closeGame() {
            game.startDemo();
            ui.setMenuVisible(true);
            gameMenuOffsetElement.classList.add('d-none');
        },
        /**
         * Handles a score event.
         * @param side Score side
         * @param ball Scoring ball
         */
        handleScore(side, ball) {
            let player = getPlayerBySide(side);
            player.scoreUp();

            // if winning score
            if (game.isWin(player.score)) {
                player.wins++;
                this.balls = [];
                this.isPlay = false;

                // delay end game
                setTimeout(function () {
                    if (!game.isPlay) {
                        game.closeGame();
                    }
                }, game.endTimeDelayMs);

            } else if (!this.isPersistentBalls && this.balls.length > this.minBalls) {
                this.balls.splice(this.balls.indexOf(ball), 1);
            } else {
                ball.reset(side);
            }
        }
    };

    /**
     * Game menu functionality
     */
    const menu = {
        /**
         * Reads default game values into HTML inputs
         */
        init() {
            winScoreInput.value = game.defaultWinScore;

            menu.initSpeedMultiplierInputs();

            // min/max, start max, persistent balls
            minBallsInput.value = game.defaultMinBalls;
            maxBallsInput.value = game.defaultMaxBalls;
            startMaxBallsInput.checked = game.defaultIsStartMaxBalls;
            persistentBallsInput.checked = game.defaultIsPersistentBalls;
        },

        /**
         * Set speed multiplier input values
         */
        initSpeedMultiplierInputs() {
            let step = .1;

            // step values
            speedMultiplierInput.step = step;
            speedMultiplierSlider.step = step;

            // input min/max values
            speedMultiplierInput.min = game.speedMultiplierMin;
            speedMultiplierInput.max = game.speedMultiplierMax;

            // slider min/max values
            speedMultiplierSlider.min = game.speedMultiplierMin;
            speedMultiplierSlider.max = game.speedMultiplierMax;

            // show min/max values
            speedMultiplierMinDisplay.textContent = game.speedMultiplierMin.toString();
            speedMultiplierMaxDisplay.textContent = game.speedMultiplierMax.toString();

            // set input and slider values
            speedMultiplierInput.value = game.defaultSpeedMultiplier;
            speedMultiplierSlider.value = game.defaultSpeedMultiplier;
        },

        /**
         * Update speed multiplier input and slider.
         * @param event Event
         */
        updateSpeedMultiplierInputs(event) {
            let target = event.target;

            if (target === speedMultiplierInput) {
                speedMultiplierSlider.value = speedMultiplierInput.value;
            } else {
                speedMultiplierInput.value = speedMultiplierSlider.value;
            }
        }
    };

    /**
     * Gets paddle closest to the specified Point object.
     * @param point A Point object
     * @returns {*} Returns the chosen paddle.
     */
    function nearestPaddle(point) {
        let closestPaddle = null;

        // if paddles exist
        if (game.paddles.length > 0) {
            closestPaddle = game.paddles[0];

            // find closest paddle
            for (let i = 1; i < game.paddles.length; i++) {
                let paddle = game.paddles[i];

                let distance1 = distance(paddle.getMiddleX(), point.x, paddle.getMiddleY(), point.y);
                let distance2 = distance(closestPaddle.getMiddleX(), point.x, closestPaddle.getMiddleY(), point.y);

                if (distance1 < distance2) {
                    closestPaddle = paddle;
                }
            }
        }
        return closestPaddle;
    }

    /**
     * Gets ball closest to specified Point object.
     * @param point A Point object
     * @param isIncomingOnly If true, only search for balls that have not passed the point.
     * @returns {*} Returns the chosen paddle.
     */
    function nearestBall(point, isIncomingOnly = false) {
        let closestBall = null;

        // if balls exist
        if (game.balls.length > 0) {
            let validBalls = [];
            if (isIncomingOnly) {
                for (let i = 0; i < game.balls.length; i++) {
                    let tempBall = game.balls[i];
                    if (isIncomingBall(tempBall, point)) {
                        validBalls.push(tempBall);
                    }
                }
            } else {
                validBalls = game.balls;
            }

            closestBall = validBalls[0];

            // find closest ball
            for (let i = 0; i < validBalls.length - 1; i++) {
                let tempBall = validBalls[i + 1];
                let distance1 = distance(closestBall.x, point.x, closestBall.y, point.y);
                let distance2 = distance(tempBall.x, point.x, tempBall.y, point.y);

                if (distance2 < distance1) {
                    closestBall = tempBall;
                }
            }
        }

        return closestBall;
    }

    /**
     * Determines if a ball is approaching and has not passed a particular point.
     * @param ball A Ball object.
     * @param point A Point object.
     * @returns {boolean} Returns true if the ball is approaching the point on the x-plane and has not passed the point.
     */
    function isIncomingBall(ball, point) {
        return (ball.x > point.x && ball.dx < 0) || (ball.x < point.x && ball.dx > 0);
    }

    /**
     * Gets a player by a side value.
     * @param side A side value.
     * @returns {*} Returns the specified player by side.
     */
    function getPlayerBySide(side) {
        for (let i = 0; i < game.players.length; i++) {
            if (game.players[i].paddle.side === side) {
                return game.players[i];
            }
        }
        return null;
    }

    /**
     * Initializes game content
     */
    function init() {
        requestAnimationFrameId = 0;
        resize();

        menu.init();
        game.play(true);
    }

    /**
     * Animation loop. Game objects updated here. Animation loop will be canceled if game.isPaused === true.
     */
    function animate() {
        requestAnimationFrameId = requestAnimationFrame(animate);

        if (game.isPaused) {
            cancelAnimationFrame(requestAnimationFrameId);
        } else {
            context.clearRect(0, 0, canvas.width, canvas.height);

            game.update();
        }
        ui.update();
    }

    /**
     * Resizes canvas to screen width and height, and adjusts font size accordingly.
     */
    function resize() {
        setCanvasSize(canvas, innerWidth, innerHeight);
        ui.init();
    }

    /**
     * Updates mouse coordinates on mousemove.
     */
    window.addEventListener('mousemove', function (event) {
        mouse.x = event.x;
        mouse.y = event.y;
    });

    /**
     * Calls init() on window load.
     */
    window.addEventListener('load', function () {
        init();
        // animate();
    });

    /**
     * Player paddle movement.
     */
    canvas.addEventListener('mousemove', function (event) {
        if (game.isPlay) {
            // iterate over paddles
            for (let i = 0; i < game.paddles.length; i++) {
                let paddle = game.paddles[i];

                // if paddle is not computer-controlled, move paddle to mouse cursor
                if (!paddle.isComputerControlled) {
                    let mousePos = mousePositionCanvas(canvas, event);

                    if (mousePos.y - paddle.height / 2 < 0) {
                        paddle.y = 0;
                    } else if (mousePos.y + paddle.height / 2 > canvas.height) {
                        paddle.y = canvas.height - paddle.height;
                    } else {
                        paddle.y = mousePos.y - paddle.height / 2;
                    }
                }
            }
        }
    });

    speedMultiplierInput.addEventListener('change', function (event) {
        menu.updateSpeedMultiplierInputs(event);
    });

    speedMultiplierSlider.addEventListener('input', function (event) {
        menu.updateSpeedMultiplierInputs(event);
    });

    resetDefaultButton.addEventListener('click', menu.init);

    demoButton.addEventListener('click', game.startDemo);

    playButton.addEventListener('click', function () {
        game.play();
        ui.setMenuVisible(false);
    });

    resumeButton.addEventListener('click', game.unpause);

    quitButton.addEventListener('click', game.closeGame);

    gameMenuCollapseButton.addEventListener('click', function () {
        ui.setMenuVisible(false);
        gameMenuOffsetElement.classList.remove('d-none');
    });

    gameMenuOffsetElement.addEventListener('click', function () {
        ui.setMenuVisible(true);
        gameMenuOffsetElement.classList.add('d-none');
    });

    /**
     * 'Pause' keypress listener
     */
    window.addEventListener('keydown', function (event) {
        let isPausable = event.key === 'Escape' && (game.isPlay && !game.isDemo);

        if (isPausable && game.isPaused) {
            game.unpause();
        } else if (isPausable) {
            game.pause();
        }
    });

}());