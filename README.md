# Customizable Pong

## Overview
* Web-ready Pong game using JavaScript and HTML canvas.
* Customizable settings.
* Play against a computer-controlled opponent or watch a computer-vs-computer match to preview your settings.
* Randomly generated color trails.

## Settings
* Win score
* Player side
* Min/max ball counts
* Ball speed multiplier
* Start max ball count
* Persistent balls (balls are never removed)

## Media
![standard](media/image/sm_demo.gif 'Standard')
![multi-ball](media/image/sm_demo_multiball.gif 'Multi-ball')

## How do I get set up?
* Download the contents of this repository.
* Load index.html in a web browser.